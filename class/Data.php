<?php
    require('SimpleXLS.php');
    
    class Data {

        private $file;

        public function __construct($file) {

            $this->file = $file;

        }

        private function getCustomFilters() {
            echo '<table id="custom_filters" class="hidden" border="0" cellspacing="5" cellpadding="5">';
            echo '<td> Né entre :</td>';
            echo '<td><input name="min" id="min" type="text"></td>';
            echo '<td>Et : </td>';
            echo '<td><input name="max" id="max" type="text"></td>';
            echo '</tr>';
            echo '</table>';
        }

        public function datasAsArray() {
            if ( $xls = SimpleXLS::parse($this->file) ) {
                echo '<table id="clients" class="hidden">';

                foreach ( $xls->rows() as $r => $row ) {
                    if($r === 0) {
                        echo "<thead><tr>";
                    
                        foreach ( $row as $c => $cell ) {
                           if($c === 0) {
                            echo '<th>#</th>';
                           } else {
                            echo '<th>'.$cell.'</th>';
                           }
                        }

                        echo "</tr></thead><tbody>";
                    } else {
                        echo "<tr>";
                    
                        foreach ( $row as $c => $cell ) {
                            if($c === 7) {
                                echo '<td class="clickable-cell" onclick="location.href=\'https://www.commune-rosieres10.fr/Recherche?id_article='.$cell.'\'";>'.$cell.'</td>';
                            } else {
                                switch ($cell) {
                                    case 'France':
                                        echo '<td class="flag-icon" style="background-image:url(\'./ressources/img/fr.svg\');">'.$cell.'</td>';
                                        break;
                                    case 'United States':
                                        echo '<td class="flag-icon" style="background-image:url(\'./ressources/img/us.svg\');">'.$cell.'</td>';
                                        break;
                                    case 'Great Britain':
                                        echo '<td class="flag-icon" style="background-image:url(\'./ressources/img/uk.svg\');">'.$cell.'</td>';
                                        break;
                                    default:
                                        echo '<td>'.$cell.'</td>';
                                        break;
                            }
                        }
                        
                    }

                        echo "</tr>";
                    }
                }

                echo "</tbody></table>";
                echo $this->getCustomFilters();

            
              } else {
                echo SimpleXLS::parseError();
              }
        }

    }