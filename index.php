<!DOCTYPE html>
<html lang="fr" class="no-scroll">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./ressources/css/datatables.min.css">
    <link rel="stylesheet" href="./ressources/css/searchpanes.min.css">
    <link rel="stylesheet" href="./ressources/css/select.min.css">
    <link rel="stylesheet" href="./ressources/css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./ressources/css/style.min.css">


    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="./ressources/js/Datatables/datatables.js"></script>
    <script src="./ressources/js/Datatables/searchPanes.min.js"></script>
    <script src="./ressources/js/Datatables/select.min.js"></script>
    <script src="./ressources/js/datepicker.js"></script>
    <script src="./ressources/js/moment.js"></script>
</head>

<body class="no-scroll">
    <header>
        <div id="banner">
            <img src="./ressources/img/logo.png" alt="logo" id="logo">
        </div>
    </header>
    <main>
        <h1 class="title">Rechercher un utilisateur</h1>
        <div id="loader" class="load-wrapp">
            <div class="load-5">
                <p>Chargement des données du tableau...</p>
                <div class="ring-2">
                    <div class="ball-holder">
                        <div class="ball"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            require('class/Data.php');

            $d = new Data('test.xls');
            $d->datasAsArray();
        ?>
    </main>

    <script>
        $(document).ready(function () {
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#min').val()
                    var max = $('#max').val()
                    var createdAt = data[6];
                    var startDate = moment(min, "DD/MM/YYYY");
                    var endDate = moment(max, "DD/MM/YYYY");
                    var diffDate = moment(createdAt, "DD/MM/YYYY");
                    if (
                        (min == "" || max == "") ||
                        (diffDate.isBetween(startDate, endDate))


                    ) { return true; }
                    return false;

                }
            );

            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true, dateFormat: "dd/mm/yy" });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true, dateFormat: "dd/mm/yy" });

            var table = $('#clients')
                .on('init.dt', function () {
                    $(this).removeClass('hidden');
                    $('#loader').addClass('hidden');
                    $('#custom_filters').removeClass('hidden');
                    $('html, body').removeClass('no-scroll');
                    $( "#custom_filters" ).insertAfter( "#clients" );

                })
                .DataTable({
                    searchPanes: {
                        layout: 'columns-7',
                    },
                    dom: 'Pfrtip',
                    "language": {
                        searchPanes: {
                            title: {
                                _: 'Filtres sélectionnés - %d',
                                0: 'Aucun filtre sélectionné',
                                1: 'Un filtre sélectionné',
                            },
                            count: '{total} résultats',
                            countFiltered: '{shown} ({total})'
                        },
                        "paginate": {
                            "first": "Première page",
                            "last": "Dernière page",
                            "previous": "Page précedente",
                            "next": "Page suivante",
                        },
                        "info": "Affichage de _START_ to _END_ résultat(s) sur _TOTAL_ entrées",
                        "lengthMenu": "Affichage de _MENU_ entrées",
                        "search": "Rechercher dans les colonnes :",
                        "infoFiltered": " - Filtré à partir de _MAX_ entrées",
                        "zeroRecords": "Aucun résultat"
                    }
                });

            $('#min, #max').change(function () {
                table.draw();
            });

        });
    </script>
</body>

</html>